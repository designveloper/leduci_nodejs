var express = require('express');
var cors = require('cors'); // support cors issue
var bodyParser = require('body-parser'); // support for POST data type
var app = express();

app.use(bodyParser.json());
app.use(bodyParser.urlencoded({ extended: false}));

app.use(function (req, res, next) { // midleware
  console.log(`${req.method} request for '${req.url}'`);
  next(); // move on to next
});

app.use(express.static("./public")); //invoke the static file server

app.use(cors());

app.get('/<routing>', function (req, res) {
    res.json(...)
});
app.listen(3000);
