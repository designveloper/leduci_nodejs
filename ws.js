//------------------------WEBSOCKET-----------------------
var WebSocketServer = require('ws').Server;
var wss = new WebSocketServer({ port: 3000 });

wss.on("connection", function (ws) {
  ws.on("message", function (message) {
    if (message === 'exit') {
      wss.close();
    } else {
      wss.clients.forEach(function (client) {  // broadcastting messages to all clients
        ws.send("<data>");
      })
    }
  })
});


//-----------------------------------------------
//On client
var ws = new WebSocket("ws://localhost:3000");

ws.onopen = function () {
  // do some stuff
};

ws.onclose = function () {
    // do some stuff
};

ws.onmessage = function (payload) {
    // do some stuff with payload.data
};

// send messages to server
ws.send("<data>")

//------------------------SOCKETIO-----------------------
var express = require('express');
var http = require('http');
var app = express();
var server = http.createServer(app).listen('<port>');

var io = require('socket.io')(server);

io.on('connection', function (socket) {
  socket.emit('<name_event>', '<data>');

  socket.on('<name_event>', function(data) {
    socket.broadcast.emit('<name_event>', data);
  })
})

// On client
var socket = io("http://localhost:port");

socket.on('disconnect', function () {

});

socket.on('connect', function () {

})

socket.on('<name_event>', function (data) {
  socket.emit('<name_event>', data)
})
