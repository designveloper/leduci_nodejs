// fileServer
var http = require('http');
var fs = require('fs');
var path = require('path');

http.createServer(function(req, res) {
  console.log(`${req.method} request for ${req.url}`);

  if (req.url === '/') {
    fs.readFile('./public/index.html', 'UTF-8', function(err, html) {
      res.writeHead(200, {'Content-Type': 'text/html'});
      res.end(html);
    })
  } else if (req.url.match(/.css$/)) {
    var cssPath = path.join(__dirname, 'public', req.url);
    var fileStream = fs.createReadStream(cssPath, "UTF-8");

    res.writeHead(200, {'Content-Type': 'text/css'});
    res.end(res);
  } else if (req.url.match(/.jpeg)) {
    var imgPath = path.join(__dirname, 'public', req.url);
    var fileStream = fs.createReadStream(imgPath);

    res.writeHead(200, {'Content-Type': 'image/jpeg'});
    res.end(res);
  } else {
    res.writeHead(404, {"Content-Type" : 'text/plain'});
    res.end('File Not Found');
  }
}).listen(3000);

// Serving JSON data
var http = require('http');
var data = require('./data.json');

http.createServer(function(req, res) {
  res.writeHead(200, {"Content-Type": "text/json"});
  res.end(JSON.stringfy(data));
}).listen(3000);

// Collecting POST data
var http = require('http');
var fs = require('fs');

http.createServer(function(req, res) {
  if (req.method === 'GET') {
    res.writeHead(200, {"Content-Type": "text/html"});
    fs.createReadStream('./public/form.html', "UTF-8").pipe(res);
  } else if (req.method === 'POST') {
      var body = '';

      req.on('data', function (chunk) {
        body += chunk;
      });

      req.on('end', function() {
        res.writeHead(200, {"Content-Type": "text/html"});
        res.end(`
          <!DOCTYPE html>
          <html>
            <body>
                ${body}
            </body>
          </html>
        `);
      })
  }
}).listen(3000);
