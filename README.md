### NODEJS  
1. Node.js Introduction  
```  
  NODEJS
    - Single thread  
    - No blocking  
    - Asynchronous -> do multiple tasks at the time
```  
2. Installation  
```  
  **Install**  
    - link: https://nodejs.org/en/download/  
    - install as usual  
    To check where nodejs is installed or not:  
    - command line:  
      node -v
      npm -v  
  **Upgrade**  
    - download and install again  
```  
3. Node Core  
```  
  **The global object**  
    - Every nodejs files own modules.
    - Any variable created in a nodejs file, is scoped only to that module.  
    - Be able to require other module into current module with keyword 'require'
    To run a file: node <name_file>.js
  **Argument variables with process.argv**  
    - process.argv is just a array
    - flag and its values saved in process.argv side by side
  **Standard input and Standard output**  
    - process.stdout
      + .write('')  
      + .cursorTo(..)  
      + .clearLine()
    - process.stdin
      + .on('data', function(data) {})  
    - process.exit()  
    - process.on('exit', function() {})    
  **Global timing functions**  
    - setTimeout(function(){}, <waiting milliseconds>);
    - var name_of_interval = setInterval(function(){}, <loop milliseconds>);
      => To clear:  
        + clearInterval(name_of_interval)
```  
4. Node Modules  
```  
  **Core modules**  
    - 'require' to load modules  
    - default modules:  
      + path  
      + util  
      + v8  
  **Collecting information with Readline**  
    - require('readline')  
    - readline.createInterface(process.stdin, process.stdout);  
    - readline.question('<question>', function(answer) {})
    - readline.setPrompt('<string>');
    - read.prompt();  
    - readline.on('line', function() {})  
  **Handling events with EventEmitter**  
    - var events = require('events')  
    - var emitter = new events.EventEmitter();  

    or EventEmitter = require('events').EventEmitter;

    - emitter.on('<name_event>', callback)  
    - emitter.emit('<name_event>', arg,...);

    Inherit other object:
      - var util = require('util');  
      util.inherits(<Target_Object>, <Object>)
  **Export and Import**  
      Export:  
        - module.exports =
        - export
      Import:  
        - using keyword: 'require'
  **Creating child process with exec/spawn**  
    Exec:  
      - var exec = require('child_process').exec;  
      - exec(...)  
    Spawn:  
      - var spawn = require('child_process').spawn;  
      - var cp = spawn("node", ["..."]);  
```  
5. The File System  
```  
  **Listing directory files**  
    - var fs = require('fs')  
          fs.readdirSync('./path')  
          fs.readdir('./path', callback)
  **Reading files**  
    - var fs = require('fs')  
          fs.readFileSync('./path/file', "UTF-8"); // default binary  
          fs.readFile('./path/file', "UTF-8", callback);
  **Writing and appending files**  
    - var fs = require('fs')  
          fs.writeFile('<file_name.ext>', <content>, callback);  
          fs.appendFile('<file_name.ext>', <content>);
  **Directory Creation**  
    - var fs = require('fs')  
          fs.mkdir('<name_directory>', callback);  
          fs.existsSync('<name_directory>')
  **Renaming and removing files**  
    - var fs = require('fs')  
          fs.renameSync('<path_file>', '<path_new_name_file>');
          fs.rename('<path_file>', '<path_new_name_file>', callback);

          try { fs.unlinkSync('<path_file>') } catch (err) {}
          fs.renunlink('<path_file>', callback);
  **Renaming and removing directories**  
    - var fs = require('fs')  
          fs.renameSync('<path_directory>', '<path_new_name_directory>');
          fs.rename('<path_directory>', '<path_new_name_directory>', callback);

          try { fs.rmdirSync('<path_directory>') } catch (err) {}
          fs.rmdir('<path_directory>', callback);


    Note: be not able to not empty directory => delete each file in folder
  **Readable/Writable file streams**  
      fs.createReadStream('<file_name>')  
      fs.createWriteStream('<file_name>')
```  
6. The HTTP Module  
```  
  **Making a request**  
    - var https = require('https');  
    - var options = {
      hostname: '<>',
      port: 443,  
      path: '...',
      method: 'GET',  
    }

    - var req = https.request(options, function(res) {
        // res.statusCode
        // res.headers  
        // res.setEncoding("UTF-8")  
        // res.once('data', callback)  
        // res.on('data', callback)  
        // res.on('end', callback)
      });  
      req.on('error', callback)  
      req.end()
  **Building a web server**  
    var http = require('http');  
    var server = http.createServer(function(req, res) {});  
      res.writeHead(<statusCode>, {"Content-Type": "text/plain"});
      res.end(<response_content>)

    server.listen(<port>);
  **Serving files**  
    var http = require('http');  
    var fs = require('fs');
    var path = require('path');

    http.createServer(function(req, res) {
      // req.url  
      // fs.readFile  
      // res.writeHead  
      // res.end(<content>)  
      // var fileStream = fs.createReadStream  
      // fileStream.pipe(res)
    }).listen(<port>);
  **Serving JSON data**  
  **Collecting POST data**
```  
7. Node Package Manager  
```  
  **Installation npm**  
    - check: npm -v  
    - command line:  
            npm install --save/--save-dev <package>  
            npm remove <package>  (--save/--save-dev)
            npm ls  

    run package local: ./node_modules/<package>/bin/<package> <command>
```  
8. Web Servers    
```  
  **The package.json file**  
    npm init /yarn init  
  **Express**  
    + Express routing  
    + CORS  
    + Express post bodies and params  
```  
9. WebSockets  
```  
  **WebSockets Server**  
    - create a two way connection to a socket  
    - post a message to the socket  
    - broadcast changes to all connections  

  **broadcast messages with WebSockets**
  **Socket.IO**  

  => SocketIO fails back to long polling when websockets aren't supported
```  
10. Testing and Debugging  
```  
  **Testing with mocha and Chai**  
    - Mocha: give a suite for describing, running and building test but it does not give a way to check values  
    - Chai: help check value  

    command line:  
        npm install chai --save-dev  
        npm install -g mocha  
  **Asynchronous mocha testing**  
    - using done arguments in callback (done())
    - this.timeout(milliseconds)  
  **Mocking a server with Nock**  
    - fake mock server  
    - command line:  
        npm install --save-dev nock  
  **Inject dependencies with rewire**  
    - command line:  
        npm install --save-dev rewire  
  **Advanced testing Sinon spies**  
    - command line:  
        npm i -S sinon
  **Code coverage with Istanbul**  
    - show how many codes covered  
    - command line:  
       npm install -g istanbul  
       istanbul cover _mocha  
  **Testing HTTP endpoints with SuperTest**  
    - command line:  
      npm install -S supertest  
  **Checking server response with Cheerio**  
    - command line:  
      npm install --save-dev cheerio  
```  
11. Automation and Deployment  
```  
  **Hinting code with Grunt**  
    - command line:
      sudo npm install -g grunt-cli  
      npm install grunt --save-dev  
      npm install grunt-contrib-jshint --save-dev  
  **Bundling client scripts with Browserify**  
```  
